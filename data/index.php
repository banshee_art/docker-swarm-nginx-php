<?php
$hostname = gethostname();
$localIP = getHostByName(getHostName());

echo "Hello World from Docker Swarm! <br>";
echo "Your container hostname: $hostname <br>";
echo "Your container local IP address: $localIP";
?>
